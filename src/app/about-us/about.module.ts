import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AboutUsComponent } from './components/about/about-us.component';
import { AboutUsRoutingModule } from './about-routing.module';

@NgModule({
  declarations: [AboutUsComponent],
  imports: [
    CommonModule,
    AboutUsRoutingModule,
  ],
})
export class AboutUsModule { }
