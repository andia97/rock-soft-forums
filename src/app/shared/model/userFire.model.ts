export interface FireUser {
    uid: string;
    email: string;
    photoUrl?: string;
    displayName?: string;
    somethingCustom?: string;
}
