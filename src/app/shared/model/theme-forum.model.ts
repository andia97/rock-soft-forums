import { Data } from '@angular/router';

export interface Theme {
    id: string;
    name: string;
    imageUrl: string;
    description: string;
    create_date: Data;
}
