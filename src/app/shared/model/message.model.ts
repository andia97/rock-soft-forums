export interface MessageTheme {
    id: string;
    themeId: string;
    uid: string;
    message: string;
    displayName: string;
    photoURL: string;
    date: Date;
}
