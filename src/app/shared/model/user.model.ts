export interface User {
    id: number;
    idTheme: number;
    // name: string;
    // password: string;
    lastName: string;
    name: string;
    image: string;
    email: string;
    date: Date;
    sex: string;
    region: string;
}
