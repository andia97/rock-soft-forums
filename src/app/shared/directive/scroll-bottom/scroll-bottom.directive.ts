import { Directive, Input, Output, ElementRef, OnInit, OnChanges, OnDestroy, AfterViewInit } from '@angular/core';
import { Subject, Observable, BehaviorSubject, fromEvent } from 'rxjs';
import { takeUntil, map, debounceTime } from 'rxjs/operators';

@Directive({
  selector: '[appScrollBottom]'
})
export class ScrollBottomDirective implements OnChanges, OnDestroy, AfterViewInit{
  // tslint:disable-next-line: no-input-rename
  @Input('stayAtBottom') stayAtBottom = true;
  // tslint:disable-next-line: no-input-rename
  @Input('scrollNow$s') scrollNow: Subject<any> = new Subject();
  // tslint:disable-next-line: no-output-rename
  @Output('amScrolledToBottom')  amScrolledToBottom: Observable<boolean> = new Observable();

  private destroy$ = new Subject();
  private changes: MutationObserver;

  private scrollEvent: Observable<{}>;
  private mutations = new BehaviorSubject(null);
  private userScrolledUp = new BehaviorSubject<boolean>(false);

  constructor(private self: ElementRef) { }

  public ngOnChanges(): void {
    if (this.stayAtBottom === true){
      this.scrollToBottom();
    }
  }

  public ngAfterViewInit(): void {
    this.registerScrollHandlers();
  }

  public ngOnDestroy(): void{
    this.destroy$.next();
    if (this.changes != null){
      this.changes.disconnect();
    }
  }
  private registerScrollHandlers(): any{
    this.amScrolledToBottom = this.userScrolledUp.pipe(takeUntil(this.destroy$), map(x => !x) );

    new MutationObserver(() => this.mutations.next(null))
      .observe(this.self.nativeElement, {
        attributes: true,
        childList: true,
        characterData: true
    });

    this.scrollEvent = fromEvent(this.self.nativeElement, 'scroll')
      .pipe(takeUntil(this.destroy$), debounceTime(100));

    this.scrollNow.pipe(takeUntil(this.destroy$)).subscribe(x => {
      this.scrollToBottom();
    });

    this.mutations.pipe(takeUntil(this.destroy$)).subscribe(x => {
      if (this.userScrolledUp.value === false){
        this.scrollToBottom();
      }
    });

    this.scrollEvent.pipe(takeUntil(this.destroy$)).subscribe(x => {
      this.setHasUserScrolledUp();
    });
  }
  private setHasUserScrolledUp(): any {

    // If currently at bottom, user not scrolled up;
    const el = this.self.nativeElement;
    if (el.scrollHeight === el.clientHeight + el.scrollTop === true) {
      this.userScrolledUp.next(false);
      return;
    }
    // If, already userScrolledUp, nothing to do.
    if (this.userScrolledUp.value === true){
      return;
    }

    // Not at bottom, is caused by mutation or user?
    // Assume mutation will scroll within 5ms.
    setTimeout(() => {
      if (el.scrollHeight === el.clientHeight + el.scrollTop === false) {
        this.userScrolledUp.next(true);
      }
    }, 5);
  }
  private scrollToBottom(): any{
    setTimeout(() => this.self.nativeElement.scrollTop = this.self.nativeElement.scrollHeight, 0);
  }

}
