import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[appHighlight]'
})
export class HighlightDirective {

  constructor(
    private element: ElementRef
    ) {
    this.element.nativeElement.style.backgroundColor = 'red';
    this.element.nativeElement.style.color = 'white';
    // this.element.nativeElement.scrollTop = this.element.nativeElement.scrollHeight;
     }

}
