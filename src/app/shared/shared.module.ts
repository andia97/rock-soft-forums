import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { RouterModule } from '@angular/router';
import { HighlightDirective } from './directive/highligh/highlight.directive';
import { ExponentialPipe } from './pipe/exponential/exponential.pipe';
import { MaterialSharedModule } from './material-module/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MessageComponent } from './components/forum/message/message.component';
import { MessageListComponent } from './components/forum/message-list/message-list.component';
import { ScrollBottomDirective } from './directive/scroll-bottom/scroll-bottom.directive';

@NgModule({
  declarations: [
    HeaderComponent,
    FooterComponent,
    HighlightDirective,
    ExponentialPipe,
    MessageComponent,
    MessageListComponent,
    ScrollBottomDirective,
  ],
  imports: [
    CommonModule,
    RouterModule,
    MaterialSharedModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [
    HeaderComponent,
    FooterComponent,
    HighlightDirective,
    ExponentialPipe,
    MessageComponent,
    MessageListComponent,
  ]
})
export class SharedModule { }
