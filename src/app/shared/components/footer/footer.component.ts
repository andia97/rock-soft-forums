import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  suscribeForm: FormGroup;
  constructor(
    private formbuilder: FormBuilder
  ) {
    this.buildForm();
  }
  ngOnInit(): void {
  }
  buildForm(): void {
    this.suscribeForm = this.formbuilder.group({
      email: [''],
    });
  }
  registerMail(): void {
    console.log('email register');
  }
}
