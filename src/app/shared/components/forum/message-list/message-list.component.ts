import { Component, OnInit, Input, OnChanges, DoCheck } from '@angular/core';
import { MessageService } from 'src/app/core/services/message/message.service';
import { MessageTheme } from 'src/app/shared/model/message.model';
import { Subject, Observable, BehaviorSubject, combineLatest, pipe } from 'rxjs';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore/';
import { switchMap, map } from 'rxjs/operators';

@Component({
  selector: 'app-message-list',
  templateUrl: './message-list.component.html',
  styleUrls: ['./message-list.component.scss']
})
export class MessageListComponent implements OnInit, OnChanges {
  @Input() themeId: string;

  private messagesCollection: AngularFirestoreCollection<MessageTheme>;
  messages$: Observable<MessageTheme[]>;

  public scrollNow = new Subject();

  constructor(
    private messageService: MessageService,
    private db: AngularFirestore,
  ) { }

  ngOnInit(): void {
    this.getMessages();
  }
  ngOnChanges(): void {
    this.getMessages();
  }
  getMessages(): any {
    // this.messagesCollection = this.messageService.getAllMessage(this.themeId);
    this.messages$ = this.db.collection('messages', ref => ref.where('themeId', '==', this.themeId)).snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data() as MessageTheme;
        const id = a.payload.doc.id;
        return { id, ...data };
      }))
    );
  }

}
