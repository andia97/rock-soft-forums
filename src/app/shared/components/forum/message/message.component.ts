import { Component, OnInit, Input, DoCheck, OnChanges } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MessageService } from 'src/app/core/services/message/message.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.scss']
})
export class MessageComponent implements OnInit, OnChanges {
  @Input() themeId: string;
  messageForm: FormGroup;
  idMessage: number;
  constructor(
    private formBuilder: FormBuilder,
    private messageService: MessageService,
    private fireAuth: AngularFireAuth,
    private fireStore: AngularFirestore,
    ) {
      this.buildForm();
      const user = this.fireAuth.authState;
      user.subscribe(res => {
        console.log(res.uid);
        this.messageForm.controls.uid.setValue(res.uid);
        this.messageForm.controls.photoURL.setValue(res.photoURL);
        this.messageForm.controls.displayName.setValue(res.displayName);
      });
    }
    ngOnInit(): void {
      this.getAllMessage();
      // console.log(this.idMessage);
    }
    ngOnChanges(): void {
      this.getAllMessage();
      // console.log(this.idMessage);
    }
    buildForm(): void {
      this.messageForm = this.formBuilder.group({
        themeId: [''],
        uid: [''],
        message: ['', [Validators.required]],
        displayName: [''],
        photoURL: [''],
        date: [new Date()],
      });
    }
    // tslint:disable-next-line: typedef
    sendMessage(): any {
      this.messageForm.controls.themeId.setValue(this.themeId);
      // console.log(this.messageForm.value);
      this.messageService.addMessage(this.messageForm.value)
      .then((res) => {
        console.log(res);
      })
      .catch((error) => {
        console.log(error);
      });
    }
    getAllMessage(): void {

    }
}
