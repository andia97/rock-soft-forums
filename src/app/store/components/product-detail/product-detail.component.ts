import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { ProductService } from 'src/app/core/services/products/product.service';
import { Product } from 'src/app/shared/model/product.model';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss']
})
export class ProductDetailComponent implements OnInit {
  product: Product;
  constructor(
    private router: ActivatedRoute,
    private productService: ProductService,
  ) { }

  ngOnInit(): void {
    this.router.params.subscribe((params: Params) => {
      // console.log(params);
      const id = params.id;
      // console.log(id);
      this.product = this.productService.getProduct(id);
      console.log(this.product);
    });
  }
  onSwiper(swiper): void {
    console.log(swiper);
  }
  onSlideChange(): void {
    console.log('slide change');
  }
}
