import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  OnChanges,
  SimpleChanges,
  DoCheck,
  OnDestroy } from '@angular/core';
import { Product } from 'src/app/shared/model/product.model';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements DoCheck, OnInit, OnDestroy {
  @Input() product: Product;
  @Output() productClicked: EventEmitter<any> = new EventEmitter();
  date: Date = new Date();
  constructor() {
    console.log('1. constructor');
  }
  // ngOnChanges(changes: SimpleChanges): void{
  //   console.log('2. ngOnChanges');
  //   console.log(changes );
  // }
  ngOnInit(): void {
    console.log('3. ngOnInit');
  }
  ngDoCheck(): void {
    // Called every time that the input properties of a component or a directive are checked. Use it to ex
    // tend change detection by performing a custom check.
    // Add 'implements DoCheck' to the class.
    console.log('4. ngDoCheck');
  }
  ngOnDestroy(): void {
    // Called once, before the instance is destroyed.
    // Add 'implements OnDestroy' to the class.
    console.log('5. ngOnDestroy');
  }
  addToCard(): void {
    console.log('add to card');
    this.productClicked.emit(this.product.id);
  }
}
