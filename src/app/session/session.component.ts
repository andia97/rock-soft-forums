import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../core/services/auth/auth.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

@Component({
  selector: 'app-session',
  templateUrl: './session.component.html',
  styleUrls: ['./session.component.scss']
})
export class SessionComponent implements OnInit {
  authForm: FormGroup;
  status: boolean;
  constructor(
    public dialogRef: MatDialogRef<SessionComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private snackBar: MatSnackBar,
    private router: Router,
  ) {
    this.buildForm();
   }

  ngOnInit(): void {
  }

  buildForm(): void {
    this.authForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email, Validators.minLength(10)]],
      password: ['', [Validators.required, Validators.minLength(8)]]
    });
  }
  signIn(): void {
    this.status = true;
    const email = this.authForm.controls.email.value;
    const password = this.authForm.controls.password.value;
    let message = '';
    this.authService.login(email, password)
    .then( (res) => {
      // console.log(res);
      // alert('(User Auth Succes');
      this.router.navigate(['/admin']);
      this.dialogRef.close();
    })
    .catch( (t) => {
      // console.log(t);
      if (t.code === 'auth/user-not-found') {
        this.authService.createUser(email, password)
        .then(
          () => {
            message = 'User register success';
            this.openSnackBar(message);
            this.router.navigate(['/admin']);
            this.dialogRef.close();
          }
        )
        .catch(
          () => {
            message = 'User Data Invalid';
            this.openSnackBar(message);
          }
        );
      } else {
        message = 'User Data Invalid';
        this.openSnackBar(message);
      }
      this.status = false;
    });
  }
  openSnackBar(message: string): void {
    this.snackBar.open(message, 'Done' , {
      duration: 3000,
      horizontalPosition: 'center',
      verticalPosition: 'top',
    });
  }
  loginWithGoogle(): void {
    let message = '';
    this.authService.loginWithGoogle()
    .then( (result) => {
      console.log(result);
      message = 'Register With Google Success';
      this.openSnackBar(message);
      this.router.navigate(['/admin']);
      this.dialogRef.close();
    })
    .catch( () => {
      message = 'Register insuccess, try again';
      this.openSnackBar(message);
    });
  }
  loginWithFacebook(): void {
    let message = '';
    this.authService.loginWithFacebook()
    .then( (result) => {
      console.log(result);
      message = 'Register With Facebook Success';
      this.openSnackBar(message);
      this.router.navigate(['/admin']);
      this.dialogRef.close();
    })
    .catch( () => {
      message = 'Register insuccess, try again';
      this.openSnackBar(message);
    });
  }
}

