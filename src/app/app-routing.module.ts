import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';

import { StoreComponent } from './store/components/store/store.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { ProductDetailComponent } from './store/components/product-detail/product-detail.component';
import { LayoutComponent } from './layout/layout.component';
import { AdminGuard } from './shared/guards/admin/admin.guard';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';

const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      {path: '', redirectTo: '/home', pathMatch: 'full' },
      {
        path: 'home',
        // component: HomeComponent
        loadChildren: () => import('./home/home.module').then((m) => m.HomeModule)
      },
      {
        path: 'store',
        // component: StoreComponent
        loadChildren: () => import('./store/store.module').then((m) => m.StoreModule)
      },
      {
        path: 'contact',
        canActivate: [AdminGuard],
        // component: ContactComponent
        loadChildren: () => import('./contact/contact.module').then((m) => m.ContactModule)
      },
      {
        path: 'blog',
        // component: BlogComponent
        loadChildren: () => import('./blog/blog.module').then((m) => m.BlogModule)
      },
      {
        path: 'about-us',
        // component: AboutUsComponent
        loadChildren: () => import('./about-us/about.module').then((m) => m.AboutUsModule)
      },
      {
        path: 'courses',
        // component: CourseComponent
        loadChildren: () => import('./course/course.module').then((m) => m.CourseModule)
      },
    ]
    // loadChildren: () => import('./landing/landing.module').then((m) => m.LandingModule)
  },
  {
    path: 'admin',
    // component: CourseComponent
    loadChildren: () => import('./admin/admin.module').then((m) => m.AdminModule)
  },
  {
    path: 'login',
    component: LoginComponent
    // loadChildren: () => import('./admin/admin.module').then((m) => m.AdminModule)
  },
  {
    path: 'register',
    component: RegisterComponent
    // loadChildren: () => import('./admin/admin.module').then((m) => m.AdminModule)
  },
  {
    path: '**',
    component: PageNotFoundComponent
    // loadChildren: () => import('./landing/landing.module').then((m) => m.LandingModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      preloadingStrategy: PreloadAllModules,
      useHash: true
    })
],
  exports: [RouterModule]
})
export class AppRoutingModule { }
