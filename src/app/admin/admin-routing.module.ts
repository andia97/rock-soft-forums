import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductFormComponent } from './components/product/product-form/product-form.component';
import { NavComponent } from './components/nav/nav.component';
import { HomeComponent } from './components/home/home.component';
import { ListProductsComponent } from './components/product/list-products/list-products.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { TreeComponent } from './components/tree/tree.component';
import { DragDropComponent } from './components/drag-drop/drag-drop.component';
import { ThemeListComponent } from './components/forum/components/theme-list/theme-list.component';
import { CreateThemeComponent } from './components/forum/components/edit-theme/create-theme.component';

const routes: Routes = [
  {
    path: '',
    component: NavComponent,
    children: [
      {path: '', redirectTo: 'home', pathMatch: 'full'},
      {path: 'home', component: HomeComponent},
      {path: 'create', component: ProductFormComponent},
      {path: 'list-product', component: ListProductsComponent},
      {path: 'dashboard', component: DashboardComponent},
      {path: 'tree', component: TreeComponent},
      {path: 'drag-drop', component: DragDropComponent},
      {
        path: 'forum',
        // component: ThemeListComponent
        loadChildren: () => import('./components/forum/forum.module') .then((m) => m.ForumModule)
      },
      {path: 'create-theme', component: CreateThemeComponent},
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
