import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { UsersComponent } from './components/users/users.component';
import { ThemeListComponent } from './components/theme-list/theme-list.component';
import { DataDiagnosticComponent } from './components/data-diagnostic/data-diagnostic.component';
import { ThemeComponent } from './components/theme/theme.component';

const routes: Routes = [
  {
    path: '',
    component: ThemeListComponent,
    children: [
      {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full',
      },
      {
        path: 'home',
        component: HomeComponent,
        children: [
          {
            path: ':id',
            component: ThemeComponent,
          },
        ]
      },
      {
        path: 'users/:id',
        component: UsersComponent,
      },
      {
        path: 'data/:id',
        component: DataDiagnosticComponent,
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ForumRoutingModule { }
