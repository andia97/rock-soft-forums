import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ForumRoutingModule } from './forum-routing.module';
import { ThemeListComponent } from './components/theme-list/theme-list.component';
import { CreateThemeComponent } from './components/edit-theme/create-theme.component';
import { HomeComponent } from './components/home/home.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
import { UsersComponent } from './components/users/users.component';
import { DataDiagnosticComponent } from './components/data-diagnostic/data-diagnostic.component';
import { MatTableModule } from '@angular/material/table';

import { NgxChartsModule } from '@swimlane/ngx-charts';
import { ThemeComponent } from './components/theme/theme.component';

@NgModule({
  declarations: [
    ThemeListComponent,
    CreateThemeComponent,
    HomeComponent,
    UsersComponent,
    DataDiagnosticComponent,
    ThemeComponent
  ],
  imports: [
    CommonModule,
    ForumRoutingModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatMenuModule,
    MatDialogModule,
    MatInputModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    MatTableModule,
    NgxChartsModule
  ]
})
export class ForumModule { }
