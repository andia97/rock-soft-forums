import { Component, OnInit } from '@angular/core';
import { single, multi } from './data';
@Component({
  selector: 'app-data-diagnostic',
  templateUrl: './data-diagnostic.component.html',
  styleUrls: ['./data-diagnostic.component.scss']
})
export class DataDiagnosticComponent implements OnInit {
  single: any[];
  multi: any[];
  view: any[] = [700, 400];
  viewBar: any[] = [1000, 400];

  colorScheme = {
    domain: ['#5AA454', '#E44D25', '#CFC0BB', '#7aa3e5', '#a8385d', '#aae3f5']
  };
  cardColor = '#232837';


   // options
   showXAxis = true;
   showYAxis = true;
   gradient = false;
   showLegend = true;
   showXAxisLabel = true;
   xAxisLabel = 'City';
   showYAxisLabel = true;
   yAxisLabel = 'Population';
   legendTitle = 'Years';
  constructor() {
    Object.assign(this, { single });
    Object.assign(this, { multi });
  }

  ngOnInit(): void {
  }
  onSelect(event): void {
    console.log(event);
  }
  onSelectBar(data): void {
    console.log('Item clicked', JSON.parse(JSON.stringify(data)));
  }

  onActivate(data): void {
    console.log('Activate', JSON.parse(JSON.stringify(data)));
  }

  onDeactivate(data): void {
    console.log('Deactivate', JSON.parse(JSON.stringify(data)));
  }
}
