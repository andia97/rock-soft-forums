import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DataDiagnosticComponent } from './data-diagnostic.component';

describe('DataDiagnosticComponent', () => {
  let component: DataDiagnosticComponent;
  let fixture: ComponentFixture<DataDiagnosticComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DataDiagnosticComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DataDiagnosticComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
