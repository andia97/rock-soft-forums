export const single = [
    {
      name: 'Cochabamba',
      value: 894
    },
    {
      name: 'Santa Cruz',
      value: 500
    },
    {
      name: 'Tarija',
      value: 720
    },
    {
      name: 'Beni',
      value: 520
    },
    {
      name: 'La Paz',
      value: 770
    },
    {
      name: 'Oruro',
      value: 430
    },
    {
      name: 'Pando',
      value: 330
    },
    {
      name: 'Chuquosaca',
      value: 230
    },
    {
      name: 'Potosi',
      value: 130
    },
  ];
export const multi = [
  {
    name: 'Pando',
    series: [
      {
        name: '2020',
        value: 730
      },
      {
        name: '2021',
        value: 894
      }
    ]
  },

  {
    name: 'Chuquisaca',
    series: [
      {
        name: '2020',
        value: 787
      },
      {
        name: '2021',
        value: 827
      }
    ]
  },

  {
    name: 'Potosi',
    series: [
      {
        name: '2020',
        value: 500
      },
      {
        name: '2021',
        value: 580
      }
    ]
  },
  {
    name: 'Santa Cruz',
    series: [
      {
        name: '2020',
        value: 157
      },
      {
        name: '2021',
        value: 125
      }
    ]
  },
  {
    name: 'Potosi',
    series: [
      {
        name: '2020',
        value: 300
      },
      {
        name: '2021',
        value: 880
      }
    ]
  },
  {
    name: 'Cochabamba',
    series: [
      {
        name: '2020',
        value: 100
      },
      {
        name: '2021',
        value: 280
      }
    ]
  },
  {
    name: 'La Paz',
    series: [
      {
        name: '2020',
        value: 200
      },
      {
        name: '2021',
        value: 380
      }
    ]
  },
  {
    name: 'Oruro',
    series: [
      {
        name: '2020',
        value: 500
      },
      {
        name: '2021',
        value: 580
      }
    ]
  },
  {
    name: 'Tarija',
    series: [
      {
        name: '2020',
        value: 182
      },
      {
        name: '2021',
        value: 236
      }
    ]
  },
];
