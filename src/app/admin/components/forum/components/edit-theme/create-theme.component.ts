import { Component, OnInit, Inject } from '@angular/core';
import { Theme } from 'src/app/shared/model/theme-forum.model';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ThemeService } from 'src/app/core/services/theme-forum/theme.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { AngularFirestore } from '@angular/fire/firestore';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-create-theme',
  templateUrl: './create-theme.component.html',
  styleUrls: ['./create-theme.component.scss']
})
export class CreateThemeComponent implements OnInit {
  themeForm: FormGroup;
  constructor(
    public dialogRef: MatDialogRef<CreateThemeComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Theme,
    private themeService: ThemeService,
    private formBuilder: FormBuilder,
    private db: AngularFirestore,
    public snackBar: MatSnackBar,
  ) {
    this.buildForm();
   }

  ngOnInit(): void {
    this.getTheme();
  }
  buildForm(): void {
    this.themeForm = this.formBuilder.group({
      name: [''],
      description: [''],
      imageUrl: [''],
    });
  }
  getTheme(): void {
    const ref = this.db.collection('theme').doc(this.data.id);
    ref.get()
    .subscribe((res) => {
      this.themeForm.patchValue(res.data());
    });
  }
  editTheme(): void {
    const newName = this.themeForm.controls.name.value;
    const newDescription = this.themeForm.controls.description.value;
    const newUrlImage = this.themeForm.controls.imageUrl.value;
    const ref = this.db.collection('theme').doc(this.data.id);
    ref.update({
      name: newName,
      description: newDescription,
      imageUrl: newUrlImage,
    })
    .then(() => {
      this.dialogRef.close();
      this.openSnackBar('Edit Success');
    })
    .catch( () => {
      this.openSnackBar('Error ocurred, please try again');
    });
  }
  openSnackBar(message: string): void {
    this.snackBar.open(message, 'Done' , {
      duration: 3000,
      horizontalPosition: 'center',
      verticalPosition: 'top',
    });
  }
}
