import { Component, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { ThemeService } from 'src/app/core/services/theme-forum/theme.service';
import { Theme } from 'src/app/shared/model/theme-forum.model';
import { MatDialog } from '@angular/material/dialog';
import { CreateThemeComponent } from '../edit-theme/create-theme.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';
import { log } from 'util';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit{
  welcomeMessage = true;
  private themesCollection: AngularFirestoreCollection<Theme>;
  themes: Observable<Theme[]>;
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );
  constructor(
    private breakpointObserver: BreakpointObserver,
    private themeService: ThemeService,
    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    private db: AngularFirestore,
  ) {}
  ngOnInit(): void {
    this.getAllTheme();
  }
  getAllTheme(): void {
    this.themesCollection = this.themeService.getAllThemes();
    this.themes = this.themesCollection.snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data() as Theme;
        const id = a.payload.doc.id;
        return { id, ...data };
      }))
    );
 }
  editTheme(idTheme: string): void {
    console.log('edit theme whit id:');
    console.log(idTheme);
    const dialogRef = this.dialog.open(CreateThemeComponent, {
      width: '400px',
      data: {id: idTheme}
    });
  }
  getTheme(id: string): void {
    console.log(id);
    // this.theme = this.themeService.getTheme(id);
  }
  deleteTheme(id: string): void {
    this.db.collection('theme').doc(id).delete();
  }
  addTheme(): void {
    const newTheme = {
      imageUrl: 'https://kinsta.com/es/wp-content/uploads/sites/8/2020/02/mejores-practicas-diseno-web-es-1024x512.jpg',
      name: 'New Theme',
      description: 'Type your description',
      create_date: new Date()
    };
    let message = '';
    this.themeService.addTheme(newTheme)
    .then( () => {
      message = 'Crete theme success';
      this.openSnackBar(message);
      // this.getAllTheme();
    })
    .catch( () => {
      message = 'Crete theme success';
      this.openSnackBar(message);
    });
  }
  openSnackBar(message: string): void {
    this.snackBar.open(message, 'Done' , {
      duration: 3000,
      horizontalPosition: 'center',
      verticalPosition: 'top',
    });
  }
}
