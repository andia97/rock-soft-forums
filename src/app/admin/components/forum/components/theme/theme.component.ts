import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ThemeService } from 'src/app/core/services/theme-forum/theme.service';
import { Theme } from 'src/app/shared/model/theme-forum.model';

@Component({
  selector: 'app-theme',
  templateUrl: './theme.component.html',
  styleUrls: ['./theme.component.scss']
})
export class ThemeComponent implements OnInit {
  themeId: string;
  theme: Theme;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private themeService: ThemeService,
  ) { }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      this.themeId = params.id;
      const ref = this.themeService.getTheme(this.themeId);
      ref.get()
      .subscribe( res => {
        this.theme = res.data();
        // console.log(res.data());
      }, error => {
        console.log(error);
      });
    });
  }

}
