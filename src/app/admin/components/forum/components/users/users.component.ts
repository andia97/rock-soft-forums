import { Component, OnInit, DoCheck } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ThemeService } from 'src/app/core/services/theme-forum/theme.service';
import { Theme } from 'src/app/shared/model/theme-forum.model';
import { User } from 'src/app/shared/model/user.model';
import { UserService } from 'src/app/core/services/user/user.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit{
  theme: Theme;
  displayedColumns: string[] = ['name', 'region', 'date', 'sex', 'action'];
  dataSource: User[];
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private themeService: ThemeService,
    private userService: UserService,
  ) { }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      // console.log(params);
      const id = + params.id;
      // console.log(id);
      // this.theme = this.themeService.getTheme(id);
      // console.log(this.theme);
    });
    // this.getUsers(this.theme.id);
  }
  // ngDoCheck(): void{
  //   // console.log('ngOnChanges');
  //   this.getUsers(this.theme.id);
  //   // console.log(this.messagesTheme);
  // }
  goToHome(): void {
    this.router.navigate(['/admin/forum']);
  }
  addNewUser(): void {
    // this.router.navigate(['/admin/forum']);
    console.log('add new user success');
  }
  deleteUser(index: number): void {
    console.log('delete user with index');
    console.log(index);
  }
  editUser(id: number): void {
    console.log('edit user with id');
    console.log(id);
  }
  getUsers(id: number): any {
    this.dataSource = this.userService.getAllUser(id);
    // console.log(this.dataSource);
  }
}
