import { Component, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { AngularFireAuth } from '@angular/fire/auth';
import { AuthService } from 'src/app/core/services/auth/auth.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {
  user: any;
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

  constructor(
    private breakpointObserver: BreakpointObserver,
    private fireAuth: AngularFireAuth,
    private authService: AuthService,
    private snackBar: MatSnackBar,
    private router: Router,
    ) {}
  ngOnInit(): void {
    this.fireAuth.authState.subscribe(
      (res) => {
        console.log(res);
        this.user = res;
    // console.log(this.user);
    });
  }
  logout(): void {
    let message = '';
    this.authService.logout()
    .then( () => {
      message = 'Logout Succes';
      this.router.navigate(['/home']);
    })
    .catch( () => {
      message = 'Logout Insuccess, try again';
    });
  }
  openSnackBar(message: string): void {
    this.snackBar.open(message, 'Done' , {
      duration: 5000,
      horizontalPosition: 'center',
      verticalPosition: 'top',
    });
  }

}
