import { Injectable } from '@angular/core';
import { MessageTheme } from '../../../shared/model/message.model';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  constructor(
    private db: AngularFirestore,
  ) { }

  getAllMessage(themeId: string): any{
    return this.db.collection<MessageTheme>('messages',
    ref => ref.where('themeId', '==', themeId));
    // messageTheme.push(this.messages.find(theme => idTheme === theme.idTheme));
  }
  addMessage(message: MessageTheme): any {
    return this.db.collection('messages').add({
      themeId: message.themeId,
      uid: message.uid,
      message: message.message,
      displayName: message.displayName,
      photoURL: message.photoURL,
      date: message.date,
    })
    .then( (result) => {
      console.log('Messgae with id: ' + result.id);
    })
    .catch( (error) => {
      console.log('Next error ocurred: ' + error);
    });
  }
}
