import { Injectable } from '@angular/core';
import { Theme } from '../../../shared/model/theme-forum.model';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class ThemeService {
  constructor(
    private db: AngularFirestore,
  ) {
  }

  getAllThemes(): any {
    return this.db.collection<Theme>('theme');
  }
  addTheme(theme: any): any{
    return this.db.collection('theme').add({
      name: theme.name,
      description: theme.description,
      imageUrl: theme.imageUrl,
      create_date: theme.create_date
    })
    .then( (result) => {
      console.log('Theme with id: ' + result.id);
    })
    .catch( (error) => {
      console.log('Next error ocurred: ' + error);
    });
  }
  getTheme(id: string): any {
    return this.db.collection('theme').doc(id);
  }
}
