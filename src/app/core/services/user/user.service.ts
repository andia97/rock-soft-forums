import { Injectable } from '@angular/core';
import { User } from '../../../shared/model/user.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  users: User[] = [
    {
      id: 1,
      idTheme: 1,
      lastName: 'Alberto',
      name: 'Andia',
      image: 's',
      email: 's',
      date: new Date(),
      sex: 'M',
      region: 'Bolivia',
    },
    {
      id: 2,
      idTheme: 1,
      lastName: 'Jimi',
      name: 'Torrico',
      image: 's',
      email: 's',
      date: new Date(),
      sex: 'M',
      region: 'Bolivia',
    },
    {
      id: 3,
      idTheme: 1,
      lastName: 'Alberto',
      name: 'Andia',
      image: 's',
      email: 's',
      date: new Date(),
      sex: 'M',
      region: 'Bolivia',
    },
    {
      id: 4,
      idTheme: 2,
      lastName: 'Alberto',
      name: 'Andia',
      image: 's',
      email: 's',
      date: new Date(),
      sex: 'M',
      region: 'Bolivia',
    },
    {
      id: 5,
      idTheme: 2,
      lastName: 'Mayra',
      name: 'Choque',
      image: 's',
      email: 's',
      date: new Date(),
      sex: 'M',
      region: 'Bolivia',
    },
    {
      id: 6,
      idTheme: 1,
      lastName: 'Carmen',
      name: 'Rodriguez',
      image: 's',
      email: 's',
      date: new Date(),
      sex: 'M',
      region: 'Bolivia',
    },
    {
      id: 7,
      idTheme: 3,
      lastName: 'Alberto',
      name: 'Andia',
      image: 's',
      email: 's',
      date: new Date(),
      sex: 'M',
      region: 'Bolivia',
    },
    {
      id: 8,
      idTheme: 1,
      lastName: 'Miguel',
      name: 'Molina',
      image: 's',
      email: 's',
      date: new Date(),
      sex: 'M',
      region: 'Bolivia',
    },
    {
      id: 9,
      idTheme: 2,
      lastName: 'Harold',
      name: 'Mejia',
      image: 's',
      email: 's',
      date: new Date(),
      sex: 'M',
      region: 'Bolivia',
    },
    {
      id: 10,
      idTheme: 3,
      lastName: 'Jessi',
      name: 'Villamor',
      image: 's',
      email: 's',
      date: new Date(),
      sex: 'M',
      region: 'Bolivia',
    },
    {
      id: 11,
      idTheme: 3,
      lastName: 'Jimi',
      name: 'Torrico',
      image: 's',
      email: 's',
      date: new Date(),
      sex: 'M',
      region: 'Bolivia',
    },
    {
      id: 12,
      idTheme: 3,
      lastName: 'Carla',
      name: 'Arevalo',
      image: 's',
      email: 's',
      date: new Date(),
      sex: 'M',
      region: 'Bolivia',
    },
  ];
  constructor() { }

  getAllUser(idTheme: number): User[]{
    const usersTheme: User[] = [];
    this.users.forEach(user => {
      if (user.idTheme === idTheme) {
        usersTheme.push(user);
      }
    });
    // messageTheme.push(this.messages.find(theme => idTheme === theme.idTheme));
    return usersTheme;
  }
  addUser(user: User): void {
    this.users.push(user);
  }
}
