import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Observable } from 'rxjs';
import firebase from 'firebase/app';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(
    private authF: AngularFireAuth
  ) {
  }
  createUser(email: string, password: string): any {
    return this.authF.createUserWithEmailAndPassword(email, password);
  }
  login(email: string, password: string): any {
    return this.authF.signInWithEmailAndPassword(email, password);
  }
  loginWithGoogle(): any {
    return this.authF.signInWithPopup(new firebase.auth.GoogleAuthProvider());
  }
  loginWithFacebook(): any {
    return this.authF.signInWithPopup(new firebase.auth.FacebookAuthProvider());
  }
  logout(): any {
    return this.authF.signOut();
  }
}
