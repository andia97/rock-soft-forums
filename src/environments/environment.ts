// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyASbjOwgyONYqJ_itxVT6QvvTpo-0sZLbE',
    authDomain: 'rock-soft.firebaseapp.com',
    projectId: 'rock-soft',
    storageBucket: 'rock-soft.appspot.com',
    messagingSenderId: '510007190927',
    appId: '1:510007190927:web:457d59aa4a13c9668f97fa',
    measurementId: 'G-GY8BXF9781'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
